#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

int color_misma_posicion(int *clave, int usuario[], int size) {
// Para los arrays puedo usar * o []
	int aux = 0;
	int cnt;
	
	for (cnt = 0; cnt < size; ++cnt)
		if (clave[cnt] == usuario[cnt])
			aux++;

	return aux;
}

void mostrar_array_int(int *a, int size) {
	int cnt; 

	for (cnt = 0; cnt < size; ++cnt)
		printf("%d%c", a[cnt], cnt + 1 != size? ',': '\n');
}

int colores_adecuados(int *clave, int *usuario, int size) {
	int aux = 0;
	int cnt_clave, cnt_usuario;

	bool utilizado[size];
	
	for (cnt_clave = 0; cnt_clave < size; ++cnt_clave)
		utilizado[cnt_clave] = false;


	for (cnt_clave = 0; cnt_clave < size; ++cnt_clave) {
		for (cnt_usuario = 0; cnt_usuario < size; ++cnt_usuario) {
			if (clave[cnt_clave] == usuario[cnt_usuario] && !utilizado[cnt_usuario]) {
				utilizado[cnt_usuario] = true;
				aux++;
				break;
			}
		}
	}


	return aux;
}

void consumir_buffer() {
	char c = ' ';

	while (c != '\n')
		scanf("%c", &c);

}

bool es_numero(char *s) {
	int cnt;

	for (cnt = 0; s[cnt] != '\0'; ++cnt)
		if (s[cnt] < '0' || s[cnt] > '9')
			return false;

	return true;
}

void leer_datos_usuario(int *a, int size) {
	char str[size+2];
	char *ptr;
	int cnt;
	while (true) {
		printf("Introduce la clave que contenga %d digitos: ", size);
		fgets(str, size+2, stdin);
		ptr = strchr(str, '\n');
		if (ptr == NULL) {
			printf("Has introducido una clave de tamaño incorrecto\n");
			consumir_buffer();
			continue;
		}
		*ptr = '\0';
		if (strlen(str) != size) {
			printf("No has introducido todos los digitos de la clave\n");
			continue;
		}
		if (!es_numero(str)) {
			printf("No has introducido una clave numerica\n");
			continue;
		}
		break;
	}
	// aqui str contiene digitos
	
	for (cnt = 0; cnt < size; ++cnt) {
		a[cnt] = str[cnt] - '0';
	}
}


void mostrar_tabla(int d1, int d2, int m[d1][d2]) {
	int row, col;

	for (row = 0; row < d1; ++row) {
		for (col = 0; col < d2; ++col)
			printf("%3d", m[row][col]);
		printf("\n");
	}
}

void generar_clave(int *c, int size) {
	int cnt;

	for (cnt = 0; cnt < size; ++cnt)
		c[cnt] = random() % 10;

}

int main() {
	const int SIZE = 4;
	const int INTENTOS = 3;

	//int clave[] = {1, 3, 1, 2};
	int clave[SIZE];
	int historial[INTENTOS][SIZE];
	int intento = 0;
	int tmp;

	srandom(time(NULL));
	generar_clave(clave, SIZE);

	for (intento = 0; intento < INTENTOS; ++intento) {
		leer_datos_usuario(&historial[intento][0], SIZE);

		mostrar_array_int(clave, SIZE);
		mostrar_array_int(&historial[intento][0], SIZE);
		tmp = color_misma_posicion( clave, &historial[intento][0], SIZE);
		if (tmp == SIZE) {
			printf("Has acertado la clave\n");
			intento++;
			break;
		} else {
			printf("Valores en posiciones correctas %d\n", tmp);
			printf("Valores correctos (independiente de la posicion) %d\n", colores_adecuados(clave, &historial[intento][0], SIZE));
		}
	}

	printf("Has realizado %d intentos:\n", intento);
	mostrar_tabla(intento, SIZE, historial);

	return 0;
}
